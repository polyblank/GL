image::https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png[Creative Commons License, link="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/"]
image::https://gitlab.com/uncoded/GL/badges/master/pipeline.svg[link=https://gitlab.com/uncoded/GL/pipelines?scope=branch]

[ https://gitlab.com/uncoded/GL/blob/master/README-fr.adoc[Version française] ]



= Software Engineering

image:https://upload.wikimedia.org/wikipedia/commons/f/f9/Universit%C3%A9_de_Strasbourg.svg[alt="University of Strasbourg logo", link=https://www.unistra.fr/]
image:https://pbs.twimg.com/profile_images/979609759665086464/pE1tfzid.jpg[alt="French-Azerbaijani University logo", link=https://www.ufaz.az/, height=256]

This document is the https://uncoded.gitlab.io/GL/en/[course material] for Software Engineering, taught since 2016 at the https://www.unistra.fr/[University of Strasbourg] and since 2020 at the https://www.ufaz.az/[French-Azerbaijani University]. +
To *read it online, click here*:
image:https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg[Read in HTML format,32,32,link=https://uncoded.gitlab.io/GL/en/,title="Read in HTML format"]
image:https://upload.wikimedia.org/wikipedia/commons/8/87/PDF_file_icon.svg[Read in PDF format,32,32,link=https://uncoded.gitlab.io/GL/en/pdf,title="Read in PDF format"]



== Description

The objectives in terms of knowledge and skills to be acquired by the student in this course are the following:

* Know how to analyze, model and develop various types of software systems;
* Be able to adapt to the various types of hardware and software in use in companies;
* Understand the importance of organizing and structuring a software development project, estimating its costs, and coordinating its implementation;
* Be able to communicate easily, both orally and in writing, the results of their analyses and work;
* Acquire working methods, a capacity for synthesis and a degree of autonomy that will enable him or her to solve various types of problems encountered in his or her professional practice, or to pursue higher education;
* Be able to adapt to the changing situations in his or her discipline and in society and to be able to contribute to their evolution.

In practice, this course provides accelerated training in the following topics:

* Business context: the actors, the stakes, some organizational aspects.
* Some essential definitions: the project, its scope, its qualities.
* The life cycles of a project, its different phases of existence.
* Overview of some analysis methods.
* Planning tools: PERT/CPM, Gantt.
* UML modeling (main diagrams only).
* Different types of tests (including: unit tests and automatic tests).
* Collaborative work through version management and continuous integration.



== Warning

This is *teaching material*, so it is not intended to _replace_ an _assiduous_ presence in class! +
It exists to give you a (rather good) idea of the different points we will discuss, to allow you to follow without having to spend too much time taking notes, and to serve as a lifeline if you have a specific impediment. +
However, as it is written in a way that is designed to give you quick access to essential information, it is not really exhaustive and contains few examples or contextual details, which are the salt of business life in general, and of software engineering in particular... For all this, come to class!



== License

* The publishing scripts are released under https://www.gnu.org/licenses/agpl-3.0.fr.html[GNU Affero GPL 3.0 license] [https://www.gnu.org/licenses/why-affero-gpl.fr.html[?]]
* The actual content of this book (text and images) is published under https://creativecommons.org/licenses/by-nc-nd/3.0/fr/[Creative Commons BY-NC-ND 3.0 license].

In short, what you have in front of you is there to teach you tricks, not to make money with it!

If you want to contribute to the improvement of this content, feel free to discuss it with me in https://gitlab.com/uncoded/GL/issues/new[a new ticket] or to propose directly your modifications in a pull request.

Thank you, and enjoy the reading! (◕‿◕✿)
