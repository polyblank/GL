
== Practical exercises

You will find below a list of practical exercises that will make you (re)discover some essential software engineering tools.
Please note that each of them has prerequisites you must satisfy _before_ you begin.

* link:scm.html[Version control]
* Unit testing link:unit_testing_python.html[Python] or link:unit_testing_java.html[Java]
* link:ci_doc.html[Documentation]
* link:BDD.html[Behaviour Driven Development]
