:source-highlighter: prettify
:source-highlighter: highlightjs

= TP : User stories + BDD
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc:
:toc-title:
:toclevels: 1



== Introduction

This practical work will make you discover development in BDD mode, or _Behaviour Driven Development_.
BDD can be used to write functional tests in natural language.
This makes it particularly interesting in the context of efficient communication with the customer.

Since BDD is only a variant of TDD, I encourage you to work in the same way
than in the previous exercise: first we take care of _writing the test_, and _then only_ the application code.

The specification of a feature we will use to illustrate BDD is as follows:
[quote, String Reversal]
____
Given a string of characters,
return the corresponding string of characters such that the word order is reversed.
The order of the letters in each word must not change.
____



== Exercise 0: Set-up

This TP requires link:https://behave.readthedocs.io/en/latest/[*behave*], a Python alternative to *Cucumber* (implemented in Ruby).
If behave is not installed on your machine, the easiest way is to install it with pip.
However, I encourage you to take advantage of the previous work and not to install behave _system-wide_.
On the contrary, install behave inside your project directory, thanks to *tox*, the configuration management tool we have link:unit_testing_python.html[already used].

I remind you the procedure:

. create a directory for your project: `tp-bdd`.
. create a `tp-bdd/tox.ini` file to configure your build with tox.
. create a `tp-bdd/setup.py` file to prepare the distribution of your project ;
  this is required by tox.
. create a subdirectory to house your python sources: `tp-bdd/src`.
. create a subdirectory to house your test sources: `tp-bdd/test`.

You are of course free to name your project directory as you wish,
but the rest of the organization described above is required.



== Exercise 1: Writing the Test

Create a _feature_ file, ending with the extension `.feature`,
and using the link:https://cucumber.io/docs/gherkin/reference/[Gherkin syntax].
This file describes the specification of a feature, as close as possible to natural language.

* A feature is represented by a `Feature:` section.
* A feature has a name and, optionally, a description.
* A feature consists of one or more scenarios.
* A scenario is represented by a `Scenario:` section.
* A scenario has a name and, optionally, a description.
* A scenario is written following the _Given, When, Then_ formalism.

Create the following file:

[source, gherkin]
.test/reverse.feature
----
Feature: Reverse the order of words in a string.
	In order to practice reading upside down,
	as a reader of any text,
	I must be able to get the words in reverse order.

	Scenario: Reversing a single string.
		Given a string reverser
		When I reverse the string "hello world"
		Then the reversed string is "world hello"
----

You can already run the tests.
If you have built your `tox.ini` correctly, just run `tox`. +
If you haven't read the documentation given above, I remind you of the command to run behave :
[source, bash]
----
behave test/
----

As it stands, the tests obviously can't pass.

Note that the output of the `behave test/` command offers to create a _steps_ file, and even offers python skeleton code for it.
This _steps_ file allows you to tool up unit tests so that the scenarios described in the `.feature` file can be used to run and validate your application code.



== Exercise 2: "Glue"

Create the steps file that allows you to run your test code in python. +
Each function annotated with `@given`, `@when` or `@then` makes behave understand that it must invoke that function whenever the corresponding natural language expression is encountered in the `.feature` file.

Each of the annotated functions takes a parameter (`context`) containing the entire test context.
You can enrich this context with variables or functions to validate your scenarios.

Here is an example of a steps file:
[source, python]
.test/steps/reverse.py
----
# -*- coding: utf-8 -*-
from behave import *
from src.reverse import Reverser

@given(u'a string reverser')
def step_impl(context):
    context.reverser = Reverser()

@when(u'I reverse the string "{}"')
def step_impl(context, s):
    context.result = context.reverser.reverse(s)


@then(u'the reversed string is "{}"')
def step_impl(context, expected):
    real = context.result
    assert real == expected, \
            'Output: "%s" (expected: "%s")' % (real, expected)
----



== Exercice 3: Implémentation

Of course, the steps file alone is not enough to pass the tests: we need a source code to test!
So, write a `src/reverse.py` source file allowing you to implement your feature.
Be careful to put the application source code file in the `src` directory, and the steps code file in the `test` directory !

If you've left wondering, here is an example consistent with the previous one:
[source, python]
.src/reverse.py
----
# -*- coding: utf-8 -*-
# `split` -> `reverse` -> `join` should do the job ಠ‿ಠ

class Reverser(object):
    def __init__(self):
        pass

    def reverse(self, s):
        return "TODO"
----

Don't forget to create an empty import file (`touch src/\__init_\_.py`).
in order to import your `Reverser` class from your `test/steps/reverse.py` file.

When your code compiles, and the tests fail, you can complete your application code (_ie._ the `src/reverse.py` file) in order to pass your test.
You can be sure that your test passes if and only if the output of `behave test/` ends with:
[source]
----
X features passed, 0 failed, 0 skipped
Y scenarios passed, 0 failed, 0 skipped
Z steps passed, 0 failed, 0 skipped, 0 undefined
----
(with X,Y,Z ≠ 0, of course)



== Exercice 4: Moar tests!

The feature as described in our `.feature` file does not cover the need.
For example, it is necessary to test the following entries: single character, multiple words, UTF8 support, palindromes, ...

Add at the end of your file the following natural language scenarios:
[source,gherkin]
.test/reverse.feature
----
	Scenario: Reversing a single string.
		Given a string reverser
		When I reverse the string "€"
		Then the reversed string is "€"

	Scenario: Reversing a single string.
		Given a string reverser
		When I reverse the string "''"'``"'"
		Then the reversed string is "''"'``"'"

	Scenario: Reversing a single string.
		Given a string reverser
		When I reverse the string "Guardians of the Galaxy"
		Then the reversed string is "Galaxy the of Guardians"

	Scenario: Reversing a single string.
		Given a string reverser
		When I reverse the string "L'Âme Sûre Ruse Mal"
		Then the reversed string is "Mal Ruse Sûre L'Âme"
----
Then, fix your application code so that all the tests pass.



== Exercise 5: Arrays

Independently testing every possible input can quickly become tedious.
That's why behave proposes to write a script with outlines (_ie._ tables), 
which allow to oppose in a readable and conceivable way the data to be tested and the corresponding expected result.

Add at the end of your file the following scenario and run your _feature_.
[source,gherkin]
.test/reverse.feature
----
    Scenario Outline: Same with outlines
        Given a string reverser
		When I reverse the string "<input data>"
		Then the reversed string is "<expected output>"

        Exemples: Chaînes
            | input data              | expected output         |
            | €                       | €                       |
            | ''"'``"'                | ''"'``"'                |
            | Guardians of the Galaxy | Galaxy the of Guardians |
            | L'Âme Sûre Ruse Mal     | Mal Ruse Sûre L'Âme     |
----
If you have done all of the above, the tests should pass without you having to implement anything more.


== Going further

behave is obviously not the only tool for writing tests in BDD.
Some examples are https://cucumber.io[Cucumber], http://concordion.org[Concordion] or http://www.fitnesse.org[FitNesse].
Each has its own advantages and disadvantages.

Personally, I find the entry barrier to using FitNesse very low.
So I suggest you test it, if you want to compare. +
It's quite fast ; you just have to:

. have a Java SDK installed,
. download the latest version of FitNesse https://fitnesse.org/FitNesseDownload[from this page],
. run the command `java -jar /path/to/fitnesse-standalone.jar [-p < a free port number>]`; 
  this command starts a web server with the fitnesse documentation,
. open your web browser and access `localhost` (at the chosen port).

As you will notice, FitNesse has a web server on board that allows you to view the test results in table format, in a similar manner to what you did in Exercise 5.


[[references]
== References

* Behave documentation: https://behave.readthedocs.io/en/latest/
* Gherkin syntax: https://cucumber.io/docs/gherkin/reference/
* The palindrome for Exercise 4 comes from https://www.senscritique.com/livre/La_Horde_du_Contrevent/233839[La Horde du Contrevent (_Windwalkers_)], from https://fr.wikipedia.org/wiki/Alain_Damasio[Alain Damasio].
+
[quote, Alain Damasio, La Horde du Contrevent]
____
¿’ Caracole: « L'âme sûre ruse mal ! » (...)

] Sélème: « L'âme sœur, elle, rue, ose mal... »
____