﻿:source-highlighter: prettify
:source-highlighter: highlightjs

= TP : Build + Tests + Intégration continue
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc:
:toc-title:
:toclevels: 1

== Prérequis

Ce TP nécessite une machine comportant des installations fonctionnelles de Java (≥1.7), https://maven.apache.org/[Apache Maven] et https://gradle.org/[Gradle].

== Introduction: FizzBuzz

FizzBuzz est un problème d'algorithmique régulièrement posé lors des entretiens d'embauche.
Il n'est pas très compliqué en soi, et permet surtout aux recruteurs de ne pas perdre de temps avec des candidats aux compétences apparaissant trop limitées en informatique.

Une description courante de ce problème est la suivante:
[quote, 'https://en.wikipedia.org/wiki/Fizz_buzz[Wikipédia]']
____
Écrire un programme qui affiche les nombres de 1 à 100.
Cependant, pour les multiples de 3, affichez "Fizz" au lieu du nombre.
Pour les multiples de 5, affichez "Buzz" au lieu du nombre.
Pour les nombres qui sont à la fois multiples de 3 et de 5, affichez "FizzBuzz".
____

Par exemple, une réponse correcte à ce problème démarrerait de la manière suivante:
[source]
----
1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz, 16, 17, Fizz, 19, Buzz, ...
----

Avant de vous précipiter pour trouver (facilement) la solution à ce problème sur Internet, je vous recommande de prendre un moment pour y réfléchir.
En effet, lors d'un entretien d'embauche, vous n'aurez peut-être que votre cerveau à votre disposition.
Alors, entraînez un peu vos méninges !

== Exercice 1: Configuration et Build Maven, Tests JUnit

https://maven.apache.org/[Apache Maven] est un outil de gestion et d'automatisation de production des projets logiciels Java.
L'objectif recherché est de produire un logiciel à partir de ses sources, en optimisant les tâches réalisées à cette fin et en garantissant le bon ordre de fabrication.
Par exemple, un processus de fabrication logique pourrait être le suivant : se procurer toutes les dépendances nécessaires à la compilation, puis (si toutes les dépendances sont satisfaite) compiler le projet, puis (si la compilation est un succès) packager le projet, puis (si le package logiciel a été créé) exécuter les tests, puis (si aucun test n'est en erreur) livrer le projet à un endroit donné, et ainsi de suite.

https://junit.org/[JUnit] est une des librairies de tests unitaires les plus connues pour le langage de programmation Java.

. Générer un projet maven:
+
[source,bash]
----
mvn archetype:generate -DgroupId=fr.unistra.fizzbuzz -DartifactId=fizzbuzz -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
----
. Observez les fichiers et les dossiers que maven a généré.
  Ou est rangé le code source ?
  Où est rangé le code de tests ?
  Quel fichier sert à configurer le build ?

. Initialisez un depôt git dans le dossier `fizzbuzz` que maven vient de créer.
  Commitez les fichiers strictement nécessaires.

. Créez le fichier `FizzBuzz.java` dans le dossier `src/main/java/fr/unistra/fizzbuzz`:
+
[source,java]
.src/main/java/fr/unistra/fizzbuzz/FizzBuzz.java
----
package fr.unistra.fizzbuzz;

public class FizzBuzz {

	public String convert(int number) {
		throw new RuntimeException("Not implemented");
	}
}
----
+
Puis, créez le fichier `FizzBuzzJUnitTest.java` dans le dossier `src/test/java/fr/unistra/fizzbuzz`:
+
[source,java]
.fizzbuzz/src/test/java/fr/unistra/fizzbuzz/FizzBuzzJUnitTest.java
----
package fr.unistra.fizzbuzz;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzJUnitTest {

	private FizzBuzz fizzbuzz = new FizzBuzz();

	@Test
	public void returnsNumberForInputNotDivisibleByThreeAndFive() {
		assertEquals("1", fizzbuzz.convert(1));
		assertEquals("2", fizzbuzz.convert(2));
		assertEquals("4", fizzbuzz.convert(4));
		assertEquals("7", fizzbuzz.convert(7));
		assertEquals("11",fizzbuzz.convert(11));
		assertEquals("13",fizzbuzz.convert(13));
		assertEquals("14",fizzbuzz.convert(14));
	}
}
----

. Buildez et testez votre projet avec la commande `mvn verify`.
  Cela ne fonctionne pas. Pourquoi ?

. Si vous implémentiez la class `FizzBuzz` maintenant, l'unique méthode de test présente, `returnsNumberForInputNotDivisibleByThreeAndFive`, ne serait pas suffisante pour garantir la qualité de votre implémentation.
  En vous basant sur la spécification du problème FizzBuzz donnée dans l'introduction de ce TP, écrivez les méthodes de test manquantes.

. Implémentez la classe `FizzBuzz`.
  Vous ne pouvez considérer votre implémentation correcte que lorsque `mvn verify` vous indiquera 0 erreurs.
  Évidemment, vous n'avez le droiti ni de supprimer, ni de commenter les tests, ni d'en passer l'exécution.

. Une fois que vous avez terminé l'implémentation et que tous les tests passent, commitez vos modifications.

== Exercice 2: Tests JUnitParams

Étant donné qu'écrire les tests a été un peu répétitif jusqu'ici, découvrons la librairie de tests JUnitParams.

. Créez le fichier `FizzBuzzJUnitParamsTest.java` dans le dossier `src/test/java/fr/unistra/fizzbuzz`:
+
[source,java]
.fizzbuzz/src/test/java/fr/unistra/fizzbuzz/FizzBuzzJUnitParamsTest.java
----
package fr.unistra.fizzbuzz;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class FizzBuzzJUnitParamsTest {

	private FizzBuzz fizzbuzz = new FizzBuzz();

	@Test
	@Parameters({"1", "2", "4", "7", "11", "13", "14"})
	public void returnsNumberForInputNotDivisibleByThreeOrFive(int number) {
		assertThat(fizzbuzz.convert(number)).isEqualTo("" + number);
	}
}
----

. Exécutez maintenant `mvn verify`.
  Le projet ne compile pas, car certaines dépendances ne sont pas trouvées.
  Pour regler le problème, il faut rajouter ces dépendances à votre fichier `pom.xml`.
  Indication: votre `pom.xml` contient déjà une dépendance: JUnit.
  Vous n'avez qu'à rajouter les dépendances ci-après en suivant le même modèle.
** https://mvnrepository.com/artifact/org.assertj/assertj-core/2.6.0[AssertJ] (pour la méthode `assertThat`)
** https://mvnrepository.com/artifact/pl.pragmatists/JUnitParams/1.0.6[JUnitParams] (pour le reste)

. Rajoutez les méthodes de test manquantes à la classe de test `FizzBuzzJUnitParamsTest`, en utilisant JUnitParams.
  Pour cela, suivez le modèle de la méthode déjà présente ainsi que celles que vous avez créées à l'exercice précédent.
  Une fois que tous vos tests passent (vos tests JUnitParams, mais aussi vos anciens tests JUnit), assurez-vous de commiter vos modifications.

== Exercice 3: Configuration et Build Gradle

Plus moderne qu'Apache Maven, https://gradle.org/[Gradle] est lui aussi un moteur de production.
Il permet de construire des projets destinés à la plateforme Java, notamment https://www.scala-lang.org/[Scala] ou http://groovy-lang.org/[Groovy].

. Créez le fichier `build.gradle` à la racine de votre dépôt (à coté de votre `pom.xml`):
+
[source]
.build.gradle
----
apply plugin: 'java'

sourceCompatibility = 1.7
targetCompatibility = 1.7

repositories {
	mavenCentral()
}

dependencies {
	testCompile "junit:junit:4.12"
	testCompile "org.assertj:assertj-core:2.6.0"
	testCompile "pl.pragmatists:JUnitParams:1.0.6"
}
----
+
Commitez-le.

. Buildez votre projet en vous servant cette fois, non pas de Maven, mais de Gradle :
+
[source]
----
gradle check
----
+
Observez les différences entre les fichiers `build.gradle` et `pom.xml`.
Comparez les étapes de build de `gradle check` et de `mvn verify`, ainsi que les artefacts produits par chacun des deux outils de build
(maven produit ses livrables dans le dossier `target`, tandis que gradle produit les siens dans le dossier `build`).
À priori, lequel de ces deux outils préféreriez-vous utiliser ?

== Exercice 4: Tests avec Mockito

À ce point du TP, la classe `FizzBuzz` est fonctionnelle et correctement testée.
Cependant, elle ne répond pas totalement au problème initial, qui spécifie d'_afficher_ les réponses correspondant aux nombres de 1 à 100.
Il est donc nécessaire d'implémenter, et donc de tester, une classe utilitaire `ProblemSolver` qui permettra d'achever le travail.

. Implémentez la classe `ProblemSolver`.
  L'implémentation doit respecter le diagramme de classes UML suivant:
+
image::TP-02.4.1.png[caption="", title="`ProblemSolver` ne connaît que les interfaces `Int2String` et `Displayer` ; il n'en connaît aucune implémentation concrête, et en particulier pas `FizzBuzz`."]

. Créez le fichier `ProblemSolverMockitoTest` dans le dossier `src/test/java/fr/unistra/fizzbuzz`.
+
[source, java]
.src/test/java/fr/unistra/fizzbuzz/ProblemSolverMockitoTest.java
----
package fr.unistra.fizzbuzz;

import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;
import static org.mockito.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ProblemSolverMockitoTest {

	@Mock
	private Int2String converter;
	@Mock
	private Displayer printer;

	@InjectMocks
	private ProblemSolver solver;

	@Test
	public void displayAHundredTimes() {
		// given
		when(converter.convert(anyInt()))
			.thenReturn("first")
			.thenReturn("other");
		// when
		solver.solve(100);
		// then
		verify(converter, times(100)).convert(anyInt());
		verify(printer, times(1)).display("first");
		verify(printer, times(99)).display("other");
		verifyNoMoreInteractions(converter, printer);
	}
}
----
+
Sans oublier de rajouter la dépendance envers la librairie mockito, lancez le build.
+
De quelle manière peut-on tester d'autres cas d'utilisation grâce aux mocks ?
+
Comparez avec ce qu'il aurait été nécessaire de faire si on avait voulu tester `ProblemSolver` indépendamment de ses dépendances, mais en utilisant un stub plutôt qu'un mock.

== Exercice 5: Intégration continue avec GitHub et Travis CI

. Si nécessaire, créez-vous un compte https://github.com/[GitHub].
  N'hésitez pas à prendre le temps de vous familiariser avec son interface, ses possibilités mais aussi ses limitations.

. Depuis votre interface GitHub, créez un nouveau dépôt vide.
  Puis, pushez-y le dépôt local que vous avez utilisé depuis le début de ce TP.

. Connectez-vous à https://travis-ci.org/[Travis CI] grâce à votre compte GitHub.
  Là encore, prenez le temps de vous familiariser avec ce service.

. Créez le fichier `.travis.yml` à la racine de votre dépôt:
+
[source, title='.travis.yml']
----
language: java
sudo: false
----
+
Commitez-le et pushez-le sur votre dépôt GitHub.

. Suivez un build de votre nouvelle intégration continue sur Travis CI.
  Assurez-vous que le build se passe bien.
  Sinon, corrigez les problèmes.

. Quel outil est utilisé par Travis CI pour builder votre projet ?
  Si c'est Gradle, comment forcer un build Maven ?
  Si c'est Maven, comment forcer un build Gradle ?
  Est-il possible d'utiliser d'autres méthodes pour builder ?

. Vous pouvez générer la documentation de votre code Java grâce à la commande suivante:
+
[source, bash]
----
javadoc -private -sourcepath "src/main/java:src/test/java" -d doc/ fr.unistra.fizzbuzz
----
+
La documentation est générée sous forme de fichiers HTML dans le dossier `doc`.
+
Faites en sorte que votre intégration continue Travis CI génère la documentation de votre projet.
Attention, la documentation ne doit être générée que si la compilation et les tests sont un succès.

. En utilisant la fonctionnalité _GitHub pages_, créez un site internet contenant la documentation de votre projet.
  Ce site doit être automatiquement et systématiquement mis à jour suite à chaque commit dans votre projet, mais seulement en cas de succès du build.
