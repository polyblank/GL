= Projet
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc:
:toc-title:
:toclevels: 2



== Introduction

Ce projet est destiné à mettre en pratique les connaissances et techniques acquises lors des cours et des TP.
Il peut cependant aussi vous permettre d'expérimenter avec de nouveaux thèmes, de nouvelles techniques et de nouveaux outils, si vous jugez ceux-ci les plus appropriés à la tâche énoncée, plus intéressants pour la carrière que vous envisagez, ou simplement plus amusants.



[[description]]
== Énoncé

Ce projet vise à proposer une implémentation de *Love Letter*, un jeu de cartes de Seiji Kanai pour 2 à 4 joueurs.
Les règles du jeu sont décrites link:http://online.fliphtml5.com/mvgr/hyvg/[dans ce PDF].
Ce document sert de spécification à ce projet.

Vous pouvez aussi les découvrir en link:https://www.youtube.com/watch?v=WAiI7G3QdOU[dans cette vidéo] de moins de 3 minutes.



== Groupes

Ce projet devra être réalisé en groupes de *8 étudiants*.
Chaque étudiant ne peut être membre que d'un seul groupe.

Essayez de faire en sorte que votre groupe soit fonctionnel, que chacun peut accomplir sa part de travail, de manière adaptée à ses envies et ses capacités.
Comme vous pourrez le remarquer lors de la lecture de ce sujet, une part importante de votre succès réside dans vos facultés d'analyse, de conception et de rédaction.
En conséquence, n'hésitez pas à jouer sur les forces de chacun.
Par exemple, si votre groupe comprend quelqu'un de moins doué pour le code mais davantage porté sur le suivi de projet et le _reporting_, il pourra être utile que cette personne se charge avec rigueur de la rédaction des rapports et du suivi des autres membres de l'équipe, quitte à n'écrire aucune ligne de code.
Le principal est de maximiser la qualité de votre projet dans sa globalité (code _et_ rapports), mais aussi d'en retirer un maximum d'éléments pour votre carrière future.

Les étudiants solo ne seront pas acceptés, et ce pour deux raisons :

* Les contraintes de planning pour les soutenances et pour la correction.
* Savoir convaincre, communiquer, partager les responsabilités, organiser des séances de travail communes bref, travailler en groupe, est un set de compétences professionnelles vital, qu'il vous faut développer.



[[modules]]
== Implémentation

L'implémentation du projet passe par deux modules obligatoires : <<module_core,noyau logiciel>> et <<module_gui,interface graphique>>.
Afin de vous permettre d'explorer certains domaines à votre convenance, et d'optimiser votre note en conséquence, ce sujet propose aussi trois modules facultatifs : <<module_ai,IA>>, <<module_network,réseau>> et <<module_plugins,extensions>>.

Les technologies utilisées sont laissées libres.
Il vous faudra cependant tenir compte des contraintes suivantes :

* Tout le groupe doit être d'accord sur le bien-fondé des technologies choisies.
  Votre projet peut évidemment utiliser plusieurs technologies différentes, mais celles-ci doivent communiquer correctement, et être appropriées à leurs rôles respectifs.
  Vos différents choix devront être motivés dans votre rapport.
* Votre projet doit pouvoir être joué depuis n'importe quelle machine équipée d'un navigateur Firefox (version 72 ou supérieure) ou Chromium (version 79 ou supérieure).



[[module_core]]
=== Noyau logiciel (Module 0)

Le projet s'appuiera sur un noyau logiciel en charge des règles du jeu.

* Les règles du jeu devront être implémentées en respectant scrupuleusement la spécification donnée dans l'énoncé.
  Vous pouvez proposer de nouvelles règles ou aménager les règles existantes, mais cela doit se faire via une ou plusieurs <<module_plugins,extensions>>.
  Les règles « de base », elles, devront toujours rester accessibles.
* L'implémentation du noyau logiciel devra être indépendante et offrir des interfaces claires avec les autres modules.
  En d'autres termes, votre implémentation devra respecter le principe de https://en.wikipedia.org/wiki/Separation_of_concerns[séparation des responsabilités] : par exemple, aucune notion d'IHM, d'IA ou de réseau ne devra transpirer dans la conception du noyau.

Comme vous le remarquerez en faisant vos recherches, les créateurs et la communauté des joueurs de Love Letter ont créé beaucoup de cartes supplémentaires pouvant être intégrées au jeu. Les cartes de base suivantes sont les seules à devoir obligatoirement être implémentées:

* Garde/_Guard_ : valeur 1, 5 exemplaires, fait perdre si devine
* Prêtre/_Priest_ : valeur 2, 2 exemplaires, regarde la carte
* Baron/_Baron_ : valeur 3, 2 exemplaires, compare les cartes
* Servante/_Handmaid_ : valeur 4, 2 exemplaires, protège
* Prince/Prince_ : valeur: 5, 2 exemplaires, fait défausser
* Roi/_King_ : valeur 6, 1 exemplaire, échange les cartes
* Comtesse/_Countess_ : valeur 7, 1 exemplaire, à défausser si Roi ou Prince
* Princesse/_Princess_ : valeur 8, 1 exemplaire, perdu si défaussé

Vous pouvez en implémenter d’autres, mais cela devra se faire via une ou plusieurs <<module_plugins,extensions>>.



[[module_gui]]
=== Interface graphique (Module 1)

Ce projet consiste réellement en une _adaptation_ des règles de Love Letter en jeu vidéo.
Comme vous le savez, l'expérience d'une jeu avec des cartes en papier autour d'une table de jeu est bien différente d'une partie « sur écran ».
Chaque support a ses forces et ses faiblesses.
Votre but pour ce projet est de tirer le meilleur parti des forces du support informatique, de sublimer l'expérience de Love Letter par le jeu vidéo.
Vous ne devez pas chercher à imiter à tout prix l'expérience originale autour d'une table, telle que celle montrée dans les vidéos données à titre d'illustration <<description,dans l'énoncé>>.

De la même manière, vous êtes absolument libres de définir les détails et le _look and feel_ de votre interface graphique.
Gardez cependant à l'esprit les points suivants :

* Le résultat final devra démontrer au maximum les qualités d'une bonne Interface Homme-Machine telles qu'abordées en cours, en particulier ergonomiques.
* Votre interface devra être localisée pour au moins deux pays parlant des langues différentes.
* Le profil de la majorité d'entre vous n'est certes pas celui d'un artiste ou d'un graphiste.
  Cependant, cela ne doit pas vous empêcher pas d'essayer de trouver des solutions techniquement à votre portée et maximisant l'attrait esthétique de votre interface.
  Concernant l'esthétique extérieure, vous n'êtes absolument pas obligés de reprendre le _look & feel_ de l'original.
  Cependant, souvenez-vous que l'apparence a une influence sur l'ergonomie : le résultat doit former un « tout » cohérent.

Si vous choisissez de n'implémenter ni <<module_ai,intelligence artificielle>> ni <<module_network,réseau>>, vous devrez proposer une solution pour permettre à plusieurs humains de jouer en local, sur le même écran.



[[module_ai]]
=== Intelligence Artificielle (Module 2, facultatif)

Ce module est facultatif.

Love Letter se joue par défaut de 2 à 4 joueurs.
Si vous voulez permettre à un joueur unique de jouer à votre application, il vous faut donc proposer au moins une Intelligence Artificielle qui prendra la place du ou des joueurs manquants.

Cette IA peut être très bête, ou à l'inverse toujours jouer de manière optimale.
Pour une meilleure expérience ludique pour le joueur humain, elle pourra aussi s'adapter à son style de jeu, afin de lui proposer un défi intéressant.

Optionnellement, il pourrait être possible de choisir parmi plusieurs IA aux styles de jeu différents.



[[module_network]]
=== Réseau (Module 3, facultatif)

Ce module est facultatif.

Love Letter se joue par défaut de 2 à 4 joueurs.
Vous pouvez choisir de permettre à plusieurs utilisateurs de lancer votre application chacun sur leur machine, et de se rejoindre pour jouer ensemble, automatiquement ou via le biais de salons ou d’un système de matchmaking, par exemple.

Le format des données échangées est laissé libre.
Cependant, le protocole choisi devrait supporter toutes les autres fonctionnalités de votre application.



[[module_plugins]]
=== Extensions (Module 4, facultatif)

Ce module est facultatif.

Comme vous le remarquerez faisant vos recherches, Love Letter est disponible en de nombreuses versions plus ou moins officielles.
Chaque version « reskinne » les cartes, altérant leur nom, leur illustration et, plus rarement, leur pouvoir.
Vous pouvez choisir de permettre ceal, et même aller plus loin, en laissant la possibilité à des _modeur_ d'inventer de nouvelles règles ou de nouveaux modes de jeu.

Ce module consiste à concevoir le noyau logiciel de manière à ce qu'il puisse accepter un certain nombre d'extensions (ou _plugins_).
La nature des extensions n'est à priori pas définie à l'avance : un moteur d'extensions bien conçu permettra d'exposer le plus grand nombre de fonctionnalités, afin que des développeurs tiers puissent les modifier ou en ajouter de nouvelles.

Les extensions ne font pas partie du code du logiciel.
À l'inverse, elles sont chargés au lancement de l'application ou dynamiquement, alors que celle-ci tourne.
Par contre, toute extension doit évidemment respecter un formalisme clairement défini lors de la conception de l'application.

Afin de démontrer l’efficacité de votre moteur, vous implémenterez une ou plusieurs extensions en explicitant leur but : nouveaux modes de jeu, ou nouvelles cartes (par exemple les link:https://tesera.ru/images/items/831604/1600x1200xsxy/photo.jpg[nouveaux personnages de l’édition premium]).

Optionnellement, vous pourrez aussi démontrer cette efficacité de manière encore plus convaincante en faisant charger par votre application une ou plusieurs extensions développés par un autre groupe.
Vous préciserez l'autre groupe dans votre rapport, et cela ne sera évidemment pas considéré comme du plagiat.
Par contre, il vous faudra vous mettre d'accord avec l'autre groupe sur les interfaces et structures de données utilisées.
Il vous faudra aussi convenir ensemble d'une méthode de publication des extensions, via un dépôt Gitlab, par exemple. +
À noter que cette méthode vaudra des points supplémentaires _aux deux groupes_ : celui qui a développé l'extension, ainsi que celui qui a supporté l'extension.
Le décompte exact des points de chaque groupe dépendra du travail fourni (si deux groupes conçoivent ensemble une ou plusieurs extensions, aucun problème, ils seront récompensés de la même manière).

Votre rapport devra bien préciser les avantages et limitations de votre moteur d'extensions.



[[reports]]
== Rapport(s)

Les différents rapports à rendre devront, entre autres, détailler et justifier les choix ergonomiques et techniques que vous aurez fait.
Leur nombre (un seul document, plusieurs, ...), leur formalisme (descriptif, _story-driven_, ...) et leur maquette sont libres.
Leur format est lui aussi libre : _HTML_, _.pdf_, _.odt_, _LaTeX_, ... votre seule contrainte est que j'arrive à le lire.
En particulier, je n'ai pas de licence Microsoft™©® Word™©®, donc évitez les _.docx_.

Quelques suggestions d'informations à publier dans votre rapport :
* les interfaces logicielles entre les différents <<modules,modules>> que votre groupe a choisi d'implémenter : services/fonctions appelés, format des données échangés, et ainsi de suite
* la manière dont vous vous êtes organisés en tant qu'équipe de développement : répartition du travail, planning et jalons, et ainsi de suite
* ce que vous avez retiré du projet : planning, réussites, difficultés, leçons apprises, experience acquise, ... Et cela, tant au niveau technique qu'humain.



[[source]]
== Livrable logiciel

Chaque groupe devra le remettre un dépôt git contenant le *code source* de son application.
Ce code source devra :

* inclure tout ce qui est nécessaire pour que je puisse le builder (le cas échéant).
   Ces informations devront être expliquées dans un README, et au mieux être exécutées par un script de build.
* permettre de produire une application tournant sans erreur sur une machine de TP ;
* faire preuve de qualités de conception logicielle telles qu'un découpage modulaire et une bonne maintenabilité.


[[presentation]]
== Soutenance

Chaque groupe présentera son travail lors d'une *soutenance*.

La soutenance pourra reprendre le contenu du ou des rapports, et/ou développer certains points d'intérêt.
Ces entretiens dureront une vingtaine de minutes, et que chaque groupe devra être présent au complet.

Là encore, essayez de voir votre soutenance comme une occasion de progresser, de vous exprimer en public, de développer vos capacités de synthèse, entre autres !



[[grades]]
== Barême

Votre logiciel devra être fonctionnel et respecter un maximum de qualités et de principes de génie logiciel.

À cet impératifs de base se rajoutent tout un tas de domaines dans lesquels vous pouvez engranger des points obtenir une bonne note.
En voici la liste non exhaustive :

* Implémenter un ou plusieurs <<modules,modules facultatifs>>.
  Consultez leur description respectives pour davantage de détails.
* Réaliser un logiciel particulièrement ergonomique et motivez vos choix dans la soutenance et le rapport.
* Réaliser un logiciel accessible aux mal voyants et/ou aux non voyants.
* Localiser votre IHM pour une culture « r2l » (par exemple arabe).
* Proposer plusieurs modes de jeu (solo, coopératif, compétitif, multijoueurs local ou réseau).
* Réaliser une application portable sur mobile (précisez les systèmes cibles dans votre rapport, et démontrez cette portabilité lors de la soutenance).
* Protéger votre application par des tests (unitaires/fonctionnels) automatisés, utiliser un serveur d'intégration continue.
* Soigner votre livraison.
* Rédiger un ou plusieurs rapports particulièrement détaillés et de qualité (schémas de conception, preuves de votre travail d'analyse, comparaison avec l'état de l'art, étude des alternatives, difficultés d'implémentation, solutions envisagées mais non retenues et pourquoi, etc).
* Proposer un manuel utilisateur convaincant, que ce soit au format « papier » (_ie._ PDF) ou intégré à votre application (manuel en ligne ou tutoriel).
* Faire une soutenance qui claque !

Évidemment, tout groupe surpris à plagier le travail d'un autre sans lui accorder le crédit qui lui est dû subira les sanctions habituelles, appliquées avec la plus extrême sévérité.